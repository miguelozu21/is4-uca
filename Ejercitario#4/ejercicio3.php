<?php
    $conn = pg_connect("host=127.0.0.1 port=5433 dbname=ejercicio1 user=postgres password=root");
	if (!$conn) { 
		echo "Ocurrio un Error al conectar";
		exit;
	}
    $result = pg_query($conn, "select p.nombre, p.precio, m.nombre , e.nombre, c.nombre
							   from producto p 
                               join marca m on p.id_marca = m.id_marca
                               join empresa e on m.id_empresa = e.id_empresa
                               join categoria c on c.id_categoria = p.id_categoria
							   ");
	if (!$result) 
	{
		"Ocurrio un Error al ejecutar la consulta";
		exit;
	}
    echo "<table>";
		echo "<tr>";
			echo "<td style='border: 1px solid black; background-color:red;'> Nom. Producto </td>";
			echo "<td style='border: 1px solid black; background-color:red;'> Precio </td>";
			echo "<td style='border: 1px solid black; background-color:red;'> Marca </td>";
			echo "<td style='border: 1px solid black; background-color:red;'> Empresa </td>";
			echo "<td style='border: 1px solid black; background-color:red;'> Categoria </td>";
		echo "</tr>";

	while ($row = pg_fetch_row($result))
	{
		echo "<tr>";
			echo "<td style='border: 1px solid black'> $row[0] </td>";
			echo "<td style='border: 1px solid black'> $row[1] </td>";
			echo "<td style='border: 1px solid black'> $row[2] </td>";
			echo "<td style='border: 1px solid black'> $row[3] </td>";
			echo "<td style='border: 1px solid black'> $row[4] </td>";
		echo "</tr>";
	}

	echo "</table>";
    pg_close($conn);

?>