<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Ejercitario#2-9">
		<title>Ejercitario#2-9</title>
	</head>
 
	<body>
	<?php
		$cantidad = 0;
		$divisor = 983;
        while (true){
          $numero=random_int(0,PHP_INT_MAX);
		  $cantidad++;
          if ($numero%$divisor==0){
			  $resultado=$numero/$divisor;
			  echo "{$numero} / {$divisor} = {$resultado} <br> {$cantidad} n&uacute;meros generados";
			  break;
          }/*else{
			  echo "{$numero} <br>";
		  }*/
        }
	?>
	</body>
</html>
