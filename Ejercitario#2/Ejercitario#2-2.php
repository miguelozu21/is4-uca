
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Ejercitario#2-2">
		<title>Ejercitario#2-2</title>
	</head>
    <style>
    p {
      font-family: Arial, Helvetica, sans-serif;
    }
	.NombreApellido{
      text-align: left;
      color: red;
      font-weight: bold;
    }
    .Nacionalidad{
      text-align: left;
      text-decoration: underline;
    }
    </style>
	<body>
		<?php
          	$nombreApellido="Miguel Ozuna";
        	$nacionalidad="Paraguaya";
        ?>
        	<p class='NombreApellido'>
        <?php
        	echo($nombreApellido);
        ?>
        	</p>
        	<p class='Nacionalidad'>
        <?php
        	print($nacionalidad);
        ?>
        	</p>
        
	</body>
</html>
