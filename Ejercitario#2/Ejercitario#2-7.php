<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Ejercitario#2-7">
		<title>Ejercitario#2-7</title>
	</head>
 
	<body>
	<?php
		$parcial1=random_int(0, 30);	
        $parcial2=random_int(0, 20);
        $final1=random_int(0, 50);
        echo("parcial1={$parcial1}; parcial2={$parcial2}; final1={$final1};<br>");
        switch (true) {
          case ($parcial1+$parcial2+$final1)<60:
              echo "Nota : 1";
              break;
          case ($parcial1+$parcial2+$final1)<70:
              echo "Nota : 2";
              break;
          case ($parcial1+$parcial2+$final1)<80:
              echo "Nota : 3";
              break;
          case ($parcial1+$parcial2+$final1)<90:
              echo "Nota : 4";
              break;
          default:
          	  echo "Nota : 5";
              break;
      	}  
	?>
	</body>
</html>
