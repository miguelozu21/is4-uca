<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Ejercitario#2-5">
		<title>Ejercitario#2-5</title>
	</head>
    <style>
    #tabla {
      font-family: Arial, Helvetica, sans-serif;
     
    }

    #tabla td, #tabla th {
      border: 1px solid #000000;
      
    }

    #tabla tr:nth-child(even){background-color: #DCDCDC;}
    .cabeceras {
      
      text-align: center;
      background-color: #C0C0C0;
      color: black;
    }
    .titulo{
      text-align: center;
      background-color: #FFFF00;
      color: black;
      border: 1px solid #000000;
    }
    </style>
	<body>
	<?php
		$tablaDelNro=9;
        ?>
        <table id="tabla">
        	<caption class="titulo">Tabla del Nro. <?php echo($tablaDelNro);?>			</caption>
            <tr>
            	<th class="cabeceras">Operaci&oacute;n</th>
            	<th class="cabeceras">Resultado</th>
          	</tr>
        <?php
        
        for($i = 1; $i <= 10; $i++) {
        	echo "<tr>";
            echo "<td>".$tablaDelNro." * {$i} ="."</td>"; 
            echo "<td>".$tablaDelNro*$i."</td>"; 
            echo "</tr>";
        }
        ?>
        </table>
        <?php
	?>
	</body>
</html>
