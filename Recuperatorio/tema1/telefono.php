<?php
class telefono{
    private int $telefono_id;
    private int $agenda_id;
    private string $telefono;
    private string $tipo;

    public function __construct($telefono_id, $agenda_id, $telefono, $tipo){
        $this->telefono_id=$telefono_id;
        $this->agenda_id=$agenda_id;
        $this->telefono=$telefono;
        $this->tipo=$tipo;
    }
    public function __tostring(){
        return  "-Telefono id: ".$this->telefono_id.
                ", Agenda id: ".$this->agenda_id.
                ", No. Telefono: ".$this->telefono.
                ", Tipo: ".$this->tipo;
    }
}
   


?>