<?php
class agenda{
    private int $agenda_id;
    private string $nombre;
    private string $apellido;
    private string $cedula;

    public function __construct($agenda_id, $nombre, $apellido, $cedula){
        $this->agenda_id=$agenda_id;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->cedula=$cedula;
    }
    public function __tostring(){
        return "-Agenda id: ".strval($this->agenda_id).
                ", Nombre: ".$this->nombre.
                ", Apellido: ".$this->apellido.
                ", C.I.: ".$this->cedula;
        
    }
}
   


?>