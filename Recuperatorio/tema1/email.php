<?php
class email{
    private int $email_id;
    private int $agenda_id;
    private string $email;

    public function __construct($email_id, $agenda_id, $email){
        $this->email_id=$email_id;
        $this->agenda_id=$agenda_id;
        $this->email=$email;
    }
    public function __tostring(){
        return "-Email id: ".$this->email_id.
                ", Agenda id: ".$this->agenda_id.
                ", Email: ".$this->email;
    }

}
   


?>