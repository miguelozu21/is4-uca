<?php
class notebook{
    private int $id;
    private int $marca_id;
    private int $pais_id;
    private string $modelo;
    private string $memoria_ram;
    private string $disco_duro;
    private int $puertos_usb;
    private string $procesador;
    public function __construct($id, $marca_id, $pais_id, $modelo, $memoria_ram, 
                                $disco_duro, $puertos_usb, $procesador){
        $this->id=$id;
        $this->marca_id=$marca_id;
        $this->pais_id=$pais_id;
        $this->modelo=$modelo;
        $this->memoria_ram=$memoria_ram;
        $this->disco_duro=$disco_duro;
        $this->puertos_usb=$puertos_usb;
        $this->procesador=$procesador;
    }
    public function __tostring(){
        return "-Notebook id: ".$this->id.
               ", Marca: ".$this->marca_id.
               ", Pais: ".$this->pais_id.
               ", Modelo: ".$this->modelo.
               ", Memoria Ram: ".$this->memoria_ram.
               ", Disco Duro: ".$this->disco_duro.
               ", Puertos USB: ".$this->puertos_usb.
               ", Procesador: ".$this->procesador
               ;
    }
    public function insertar($id, $nombre){
        try{
            //este proc no esta terminado
                $conn = new PDO("pgsql:host=localhost; dbname=finalTercera; port=5433;","postgres","root");
               ", Memoria Ram: ".$this->memoria_ram.
               $sql = "insert into notebooks (id, marca_id, pais_id, modelo, memoria_ram
                                            -disco_duro , 
                                            puertos_usb, procesador
                                            ) values(:id, :nombre)";
            $statement = $conn->prepare($sql);
            $statement->bindparam(":id",$id,PDO::PARAM_INT);
            $statement->bindparam(":nombre",$nombre,PDO::PARAM_STR);
            $statement->execute();

            $conn = null;
        }catch(PDOException $e){
            echo "ERROR: ". $e->getMessage();
        }
        
    }

}

?>