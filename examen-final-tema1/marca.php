<?php
class marca{
    private int $id;
    private string $nombre;

    public function __construct($id, $nombre){
        $this->id=$id;
        $this->nombre=$nombre;
    }
    public function __tostring(){
        return "-Marca id: ".$this->id.
               ", Nombre: ".$this->nombre;
    }
    public function getId(){
        return $this->id;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function insertar($id, $nombre){
        try{
            $conn = new PDO("pgsql:host=localhost; dbname=finalTercera; port=5433;","postgres","root");
            $sql = "insert into marcas (id, nombre) values(:id, :nombre)";
            $statement = $conn->prepare($sql);
            $statement->bindparam(":id",$id,PDO::PARAM_INT);
            $statement->bindparam(":nombre",$nombre,PDO::PARAM_STR);
            $statement->execute();

            $conn = null;
        }catch(PDOException $e){
            echo "ERROR: ". $e->getMessage();
        }
        
    }
}

?>