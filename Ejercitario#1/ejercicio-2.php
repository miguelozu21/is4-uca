<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Tabla de Productos">
		<title>Tabla de Productos</title>
	</head>
    <style>
    #productos {
      font-family: Arial, Helvetica, sans-serif;
     
    }

    #productos td, #productos th {
      border: 1px solid #000000;
      
    }

    #productos tr:nth-child(even){background-color: #DCDCDC;}
    .cabeceras {
      
      text-align: center;
      background-color: #C0C0C0;
      color: black;
    }
    .titulo{
      text-align: center;
      background-color: #FFFF00;
      color: black;
      border: 1px solid #000000;
    }
    </style>
	<body>
	<?php
		$array = array(array(
                       "Nombre" => "Coca Cola",
                       "Cantidad" => 100,
                       "Precio" => "4.500"
                     	),
                        array(
                       "Nombre" => "Pepsi",
                       "Cantidad" => 30,
                       "Precio" => "4.800"
                     	),
                         array(
                       "Nombre" => "Sprite",
                       "Cantidad" => 20,
                       "Precio" => "4.500"
                     	),
                        array(
                       "Nombre" => "Guaran&aacute;",
                       "Cantidad" => 200,
                       "Precio" => "4.500"
                     	),
                        array(
                       "Nombre" => "SevenUp",
                       "Cantidad" => 24,
                       "Precio" => "4.800"
                     	),
                        array(
                       "Nombre" => "Mirinda Naranja",
                       "Cantidad" => 56,
                       "Precio" => "4.800"
                     	),
                         array(
                       "Nombre" => "Mirinda Guaran&aacute;",
                       "Cantidad" => 89,
                       "Precio" => "4.800"
                     	),
                         array(
                       "Nombre" => "Fanta Naranja",
                       "Cantidad" => 10,
                       "Precio" => "4.500"
                     	),
                         array(
                       "Nombre" => "Fanta Pi&ntilde;a",
                       "Cantidad" => 2,
                       "Precio" => "4.500"
                     	),
                    );
                     
        ?>
        <table id="productos">
        	<caption class="titulo">Productos</caption>
            <tr>
            	<th class="cabeceras">Nombre</th>
            	<th class="cabeceras">Cantidad</th>
            	<th class="cabeceras">Precio (Gs)</th>
          	</tr>
        <?php
        
        foreach( $array as $indice => $producto ){
        	echo "<tr>";
            foreach($producto as $key => $value){
            	echo "<td>".$value."</td>"; 
            }
            echo "</tr>";
        }
        ?>
        </table>
        <?php
	?>
	</body>
</html>
