<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Ejercitario#3-Ej.2">
		<title>Ejercitario#3-Ej.2</title>
	</head>
	<body>
	<h1>
	<?php
		$VERSION=PHP_RELEASE_VERSION ;
		$VERSION_ID=PHP_VERSION_ID ;
		$PHP_INT_MAX=PHP_INT_MAX;
		$PHP_MAXPATHLEN=PHP_MAXPATHLEN ;
		$PHP_OS=PHP_OS;
		$PHP_EOL=strval(PHP_EOL);
		$DEFAULT_INCLUDE_PATH =DEFAULT_INCLUDE_PATH ;
        echo "Version={$VERSION} <br>";
		echo "Version ID={$VERSION_ID} <br>";
		echo "M&aacute;ximo Valor entero permitido={$PHP_INT_MAX}<br>";
		echo "Tama&ntilde;o m&aacute;ximo de nombre de archivo permitido={$PHP_MAXPATHLEN}<br>";
		echo "Versi&oacute;n de SO={$PHP_OS}<br>";
		echo "S&iacute;mbolo de fin de l&iacute;nea={$PHP_EOL}<br>";
		echo "Include path por defecto={$DEFAULT_INCLUDE_PATH}";
	?>
	</h1>
	</body>
</html>