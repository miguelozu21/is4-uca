<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Ejercitario#3-Ej.3">
		<title>Ejercitario#3-Ej.3</title>
	</head>
	<body>
	<?php
		const N = 100;
		?>
			<table id="numeros">
        	<caption class="titulo">N&uacute;meros Pares hasta el <?php echo N; ?></caption>
            <tr>
            	<th class="cabeceras">N&uacute;mero</th>
          	</tr>
		<?php
			for ($i = 1; ; $i++) {
				if ($i > N) {
					break;
				}else{
					if($i%2==0){
						echo "<tr>";
						echo "<td>".$i."</td>"; 
						echo "</tr>";
					}
				}	
			}
			?>
			</table>
			<?php

			?>
	</body>
</html>